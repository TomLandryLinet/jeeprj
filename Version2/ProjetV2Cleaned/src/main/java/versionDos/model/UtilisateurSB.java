/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package versionDos.model;

import java.util.ArrayList;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Alexis
 */
@Stateless
public class UtilisateurSB {
    @PersistenceContext
    EntityManager myEntityManager;
    
    ArrayList<Utilisateur> listUsers;
    
    /**
     * Get the list of all user in the database
     * @return
     * The list of all user
     */
    public ArrayList<Utilisateur> getUtilisateurs(){
        listUsers = new ArrayList<>();
        Query myQuery = myEntityManager.createNamedQuery("Utilisateur.findAll");
        listUsers.addAll(myQuery.getResultList());
        return listUsers;
    }
    
    
    /**
     * Return a User if exist in the database search by login and password
     * @param login
     * @param pass
     */
    public Utilisateur checkIfLoginAndPasswordAreRight(String login, String pass){
        ArrayList<Utilisateur> allUsers = getUtilisateurs();
        Utilisateur returnedUser = null;
        for(int i = 0; i < allUsers.size(); i++){
            if(allUsers.get(i).getLogin().equals(login) && allUsers.get(i).getPassword().equals(pass)){
                returnedUser = allUsers.get(i);
            }
        }
        return returnedUser;
    }
}
