/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package versionDos.model;

import java.util.ArrayList;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Alexis
 */
@Stateless
public class EmployesSB {

    @PersistenceContext
    EntityManager myEntityManager;
    
    ArrayList<Employes> listEmployees;
    
    /**
     * Get the list of all employees
     */
    public ArrayList<Employes> getEmployes(){
        listEmployees = new ArrayList<>();
        Query myQuery = myEntityManager.createNamedQuery("Employes.findAll");
        listEmployees.addAll(myQuery.getResultList());
        return listEmployees;
    }
    
    /**
     * Get an employee, search by id
     * @param id
     */
    public Employes getEmployesByID(String id){
        if(id != null){
            Query myQuery = myEntityManager.createNamedQuery("Employes.findById");
            myQuery.setParameter("id", Integer.parseInt(id));
            return (Employes) myQuery.getSingleResult();
        }
        return null;
    }
    
    /**
     * Delete an employee, search by id
     * @param id
     */
    public void deleteEmployesByID(String id){
        Query myQuery = myEntityManager.createNamedQuery("Employes.deleteById");
        myQuery.setParameter("id", Integer.parseInt(id));
        myQuery.executeUpdate();
    }
    
    /**
     * Save (insert like) an employe
     * @param emp
     */
    public void saveEmployee(Employes _employe){
        myEntityManager.persist(_employe);
    }

    /**
     * Update (Update like) an employe
     * @param emp
     */
    public void updateEmployee(Employes _employe){
        myEntityManager.merge(_employe);
    }
}
