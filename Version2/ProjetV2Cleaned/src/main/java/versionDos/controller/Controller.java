/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package versionDos.controller;


import static versionDos.extra.Constantes.*;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import versionDos.model.Employes;
import versionDos.model.EmployesSB;
import versionDos.model.Utilisateur;
import versionDos.model.UtilisateurSB;
import javax.ejb.EJB;

/**
 *
 * @author Alexis
 */
public class Controller extends HttpServlet{
    
    HttpSession session;
    String dBURL;
    String dBUser;
    String dBPassword;
    
    @EJB
    private EmployesSB employeSB;
    
    @EJB
    private UtilisateurSB userSB;
        
    Utilisateur selectedUser;
    Employes selectedEmployee;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        session = request.getSession();
        
        // Retrieve user from the session
        selectedUser = (Utilisateur) session.getAttribute("user");
        if(null == request.getParameter("action")) {
            //If user is connected
            if (selectedUser != null) {
                request.setAttribute("employeesList", employeSB.getEmployes());
                request.getRequestDispatcher(WELCOMEPAGE_JSP).forward(request, response);
            } else {
                request.getRequestDispatcher(LOGINPAGE_JSP).forward(request, response);
            }
        } 
        // If the use try to connect
        else // If the form is empty
        switch (request.getParameter("action")) {
            case "login":
                if (request.getParameter("login").isEmpty() || request.getParameter("password").isEmpty()) {
                    request.setAttribute("fieldIsEmpty", FIELDEMPTY_ERROR_MESSAGE);
                    request.getRequestDispatcher(LOGINPAGE_JSP).forward(request, response);
                } else {
                    Utilisateur userSlected = userSB.checkIfLoginAndPasswordAreRight(request.getParameter("login"),request.getParameter("password"));
                    // Check is the credentials are valid combinaison from db
                    if (userSlected != null) {
                        selectedUser = userSlected;
                        session.setAttribute("user", selectedUser);
                        request.setAttribute("employeesList", employeSB.getEmployes());
                        request.getRequestDispatcher(WELCOMEPAGE_JSP).forward(request, response);
                    } else {
                        request.setAttribute("wrongLoginPassword", WRONGLONGINS_ERROR_MESSAGE);
                        request.getRequestDispatcher(LOGINPAGE_JSP).forward(request, response);
                    }
                }   break;
            case "details":
                if (selectedUser != null) {
                    selectedEmployee = employeSB.getEmployesByID(request.getParameter("id"));
                    if (selectedEmployee != null) {
                        request.setAttribute("selectedEmployee", selectedEmployee);
                        request.setAttribute("employeesList", employeSB.getEmployes());
                        request.getRequestDispatcher(ADDORUPDATEPAGE_JSP).forward(request, response);
                    } else {
                        request.setAttribute("userUnselectedForDetails", USERUNSELECTEDFORDETAILS_ERROR_MESSAGE);
                        request.setAttribute("employeesList", employeSB.getEmployes());
                        request.getRequestDispatcher(WELCOMEPAGE_JSP).forward(request, response);
                    }
                } else {
                    request.getRequestDispatcher(LOGINPAGE_JSP).forward(request, response);
                }   break;
            case "supprimer":
                if (selectedUser != null) {
                    // We check if the user is admin, else we redirect it to the welcome page
                    if (selectedUser.getIsAdmin()) {
                        if(request.getParameter("id") != null){
                            employeSB.deleteEmployesByID(request.getParameter("id"));
                            request.setAttribute("deletionSucceed", DELETION_SUCCEEDED_MESSAGE);
                            request.setAttribute("employeesList", employeSB.getEmployes());
                            request.getRequestDispatcher(WELCOMEPAGE_JSP).forward(request, response);
                        }
                        else {
                            request.setAttribute("userUnselectedForDeletion", USERUNSELECTEDFORDELETION_ERROR_MESSAGE);
                            request.setAttribute("employeesList", employeSB.getEmployes());
                            request.getRequestDispatcher(WELCOMEPAGE_JSP).forward(request, response);
                        }
                    } else {
                        request.setAttribute("employeesList", employeSB.getEmployes());
                        request.getRequestDispatcher(WELCOMEPAGE_JSP).forward(request, response);
                    }
                } else {
                    request.getRequestDispatcher(LOGINPAGE_JSP).forward(request, response);
                }   break;
            case "ajouter":
                if (selectedUser != null) {
                    if (selectedUser.getIsAdmin()) {
                        request.getRequestDispatcher(ADDORUPDATEPAGE_JSP).forward(request, response);
                    } else {
                        request.setAttribute("employeesList", employeSB.getEmployes());
                        request.getRequestDispatcher(WELCOMEPAGE_JSP).forward(request, response);
                    }
                } else {
                    request.getRequestDispatcher(LOGINPAGE_JSP).forward(request, response);
                }   break;
            case "deconnexion":
                session.invalidate();
                request.getRequestDispatcher(JSP_EXIT_PAGE).forward(request, response);
                break;
            case "modifier":
                if (selectedUser != null) {
                    if (selectedUser.getIsAdmin()) {
                        selectedEmployee = new Employes();
                        selectedEmployee.setId(Integer.parseInt(request.getParameter("id")));
                        selectedEmployee.setPrenom(request.getParameter("prenom"));
                        selectedEmployee.setNom(request.getParameter("nom"));
                        selectedEmployee.setTeldom(request.getParameter("telDom"));
                        selectedEmployee.setTelport(request.getParameter("telPort"));
                        selectedEmployee.setTelpro(request.getParameter("telPro"));
                        selectedEmployee.setAdresse(request.getParameter("adresse"));
                        selectedEmployee.setCodepostal(request.getParameter("codePostal"));
                        selectedEmployee.setVille(request.getParameter("ville"));
                        selectedEmployee.setEmail(request.getParameter("email"));
                        employeSB.updateEmployee(selectedEmployee);
                        request.setAttribute("employeesList", employeSB.getEmployes());
                        request.getRequestDispatcher(WELCOMEPAGE_JSP).forward(request, response);
                    }
                } else {
                    request.getRequestDispatcher(LOGINPAGE_JSP).forward(request, response);
                }   break;
            case "valAjout":
                if (selectedUser != null) {
                    if (selectedUser.getIsAdmin()) {
                        selectedEmployee = new Employes();
                        selectedEmployee.setPrenom(request.getParameter("prenom"));
                        selectedEmployee.setNom(request.getParameter("nom"));
                        selectedEmployee.setTeldom(request.getParameter("telDom"));
                        selectedEmployee.setTelport(request.getParameter("telPort"));
                        selectedEmployee.setTelpro(request.getParameter("telPro"));
                        selectedEmployee.setAdresse(request.getParameter("adresse"));
                        selectedEmployee.setCodepostal(request.getParameter("codePostal"));
                        selectedEmployee.setVille(request.getParameter("ville"));
                        selectedEmployee.setEmail(request.getParameter("email"));
                        employeSB.saveEmployee(selectedEmployee);
                        request.setAttribute("employeesList", employeSB.getEmployes());
                        request.getRequestDispatcher(WELCOMEPAGE_JSP).forward(request, response);
                    }
                } else {
                    request.getRequestDispatcher(LOGINPAGE_JSP).forward(request, response);
                }   break;
            default:
                break;
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns description of the servlet.
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
