<%-- 
    Document   : Logout
    Created on : 19 nov. 2019, 22:43:15
    Author     : Alexis
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="logout_page">
            <h1 style="">Deconnexion réussie</h1>
            <h4>Si vous désirez-vous reconnecter,
                <form method="POST" action="Control">
                <button class="btn btn-primary rounded mr-2" type='submit' name="reLogIn">clik ici</button>
                </form>
            </h4>
        </div>
    </body>
</html>
