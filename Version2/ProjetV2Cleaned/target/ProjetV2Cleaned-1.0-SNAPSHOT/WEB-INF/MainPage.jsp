<%-- 
    Document   : MainPage
    Created on : 17 nov. 2019, 22:53:03
    Author     : Alexis
--%>


<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Bienvenue sur l'interface employé</title>
    </head>
    <body>
        <nav class="navbar bg-light">
            <div class=" w-100 text-right">
            <c:if  test="${user ne null}">
                <p>Bonjour <b>${ user.login}</b> ! Votre session est active </p>
            </c:if>
                <form method="POST" action="Control">
                    <button class="btn btn-dark" type="submit" name="action" value="deconnexion">X</button>
                </form>
            </div>
        </nav>
        <c:choose>
            <c:when test="${fn:length(employeesList) > 0}">
        <div class="container">
            <h4 class="pt-4">Liste des employés</h4>
            
            <form method="POST" action="Control">
            <div class="table-responsive">
            <table class="table table-striped mb-0">
                <thead class="table-borderless">
                    <tr>
                        <th>Sel</th>
                        <th>NOM</th>
                        <th>PRENOM</th>
                        <th>TEL. DOMICILE</th>
                        <th>TEL PORTABLE</th>
                        <th>TEL PRO</th>
                        <th>ADRESSE</th>
                        <th>CODE POSTAL</th>
                        <th>VILLE</th>
                        <th>EMAIL</th>
                    </tr>
                </thead>
                <tbody>
                        <c:forEach items="${requestScope.employeesList}" var="emp">
                            <tr>
                                <th><input type="radio" name="id" value="${emp.id}"></th>
                                <td>${emp.nom}</td>
                                <td>${emp.prenom}</td>
                                <td>${emp.teldom}</td>
                                <td>${emp.telport}</td>
                                <td>${emp.telpro}</td>
                                <td>${emp.adresse}</td>
                                <td>${emp.codepostal}</td>
                                <td>${emp.ville}</td>
                                <td>${emp.email}</td>
                            </tr>
                        </c:forEach>  
                </tbody>
                </table>
                </div>
                <div class="pt-4">
                    <button class="btn btn-primary rounded mr-2" type='submit' name="action" value="supprimer" <c:if  test="${!user.isAdmin}">Disabled </c:if>>Supprimer</button>
                <button class="btn btn-primary rounded mr-2" type='submit' name="action" value="details" <c:if  test="${!user.isAdmin}">Disabled </c:if>>Details</button>
                <button class="btn btn-light rounded" type='submit' name="action" value="ajouter" <c:if  test="${!user.isAdmin}">Disabled </c:if>>Ajouter</button>
                <c:if test="${!empty deletionSucceed}">
                    <h3 class="text-success"> ${ deletionSucceed}</h3>
                </c:if>
                <c:if test="${!empty userUnselectedForDeletion}">
                    <h3 class="text-danger"> ${ userUnselectedForDeletion}</h3>
                </c:if>
                <c:if test="${!empty userUnselectedForDetails}">
                    <h3 class="text-danger"> ${ userUnselectedForDetails}</h3>
                </c:if>
                
                </div>
            </form>
        </div>
                </c:when>
                <c:otherwise>
                    <h1>Nous devons recruter !</h1>
                    <form method="POST" action="Control">
                        <div class="pt-4">
                            <button type='submit' name="action" value="add">Add</button>
                        </div>
                    </form>
                </c:otherwise>
            </c:choose>
    </body>
</html>