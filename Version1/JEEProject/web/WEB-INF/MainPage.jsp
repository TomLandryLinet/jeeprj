<%-- 
    Document   : MainPage
    Created on : 17 nov. 2019, 22:53:03
    Author     : Alexis
--%>

<%@page import="ElPackegito.Model.Employee"%>
<%@page import="ElPackegito.Model.User"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Bienvenue sur l'interfaceemployé</title>
    </head>
    <body>
        <nav class="navbar bg-light">
            <% User u = (User) request.getSession().getAttribute("user");
            String isDeleteSucess = (String) request.getAttribute("deleteSucceed");
            String isDisabled = "disabled";
            if(u.getIsAdmin()) isDisabled = "";
            %>
            <div class=" w-100 text-right">Bonjour <b><%=u.getLogin()%></b> ! Votre session est active 
                <form method="POST" action="Control">
                    <button class="btn btn-dark" type="submit" name="action" value="logOut">X</button>
                </form>
            </div>
        </form>
        </nav>
        <div class="container">
            <h4 class="pt-4">Liste des employés</h4>
            <form method="POST" action="Control">
            <%
            ArrayList<Employee> employees = (ArrayList<Employee>) request.getAttribute("employeesList");
            if (employees.size() > 0){
            %>
            <div class="table-responsive">
            <table class="table table-striped mb-0">
                <thead class="table-borderless">
                    <tr>
                        <th>Sel</th>
                        <th>NOM</th>
                        <th>PRENOM</th>
                        <th>TEL. DOMICILE</th>
                        <th>TEL PORTABLE</th>
                        <th>TEL PRO</th>
                        <th>ADRESSE</th>
                        <th>CODE POSTAL</th>
                        <th>VILLE</th>
                        <th>EMAIL</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        for (int i = 0; i < employees.size(); i++) {
                    %>
                        <tr>
                            <th><input type="radio" name="id" value="<%=employees.get(i).getId()%>"></th>
                            <td><%=employees.get(i).getNom()%></td>
                            <td><%=employees.get(i).getPrenom()%></td>
                            <td><%=employees.get(i).getTelDom()%></td>
                            <td><%=employees.get(i).getTelPort()%></td>
                            <td><%=employees.get(i).getTelPro()%></td>
                            <td><%=employees.get(i).getAdresse()%></td>
                            <td><%=employees.get(i).getCodePostal()%></td>
                            <td><%=employees.get(i).getVille()%></td>
                            <td><%=employees.get(i).getEmail()%></td>
                        </tr>
                    <%}%>
                </tbody>
                </table>
                </div>
                <div class="pt-4">
                <button class="btn btn-primary rounded mr-2" type='submit' name="action" value="supprimer" <%=isDisabled%>>Supprimer</button>
                <button class="btn btn-primary rounded mr-2" type='submit' name="action" value="details">Details</button>
                <button class="btn btn-light rounded" type='submit' name="action" value="ajouter" <%=isDisabled%>>Ajouter</button>
                <% 
                        if(isDeleteSucess.equals("1")){%>
                        <h3 class="text-success"> Suppression réussie !</h3>
                <%      }
                        if(isDeleteSucess.equals("0")){%>
                        <h3 class="text-danger">Veuillez sélectionner l'employé(e) à supprimer</h3>
                <%      }
                        if(isDeleteSucess.equals("-2")){%>
                        <h3 class="text-danger">Veuillez sélectionner l'employé(e) afin d'obtenir des détails</h3>
                <%      }
                    } 
                    else {%>
                
                    <h1>Nous devons recruter !</h1>
                    <form method="POST" action="Control">
                        <div class="pt-4">
                            <button type='submit' name="action" value="add">Add</button>
                        </div>
                    </form>
                <%}%>
                </div>
            </form>
        </div>
    </body>
</html>