/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ElPackegito.Extra;

/**
 *
 * @author Alexis
 */
public class Constantes {
    
    public static final String DATABASE_URL = "jdbc:derby://localhost:1527/JEEPRJ";
    public static final String DATABASE_LOGIN = "jee";
    public static final String DATABASE_PASSWORD = "jee";
    
    /**
     * The SQL query to select all Users
     */
    public static final String SELECT_ALL_USERS = "SELECT * FROM UTILISATEUR";

    /**
     * The SQL query to select all Employees
     */
    public static final String SELECT_ALL_EMPLOYES = "SELECT * FROM EMPLOYES";
    
    /**
     * The SQL query to select employee by id
     */
    public static final String SELECT_EMPLOYE_BY_ID = "SELECT * FROM EMPLOYES WHERE ID = ?";

    /**
     * The SQL query to delete an employee by its id
     */
    public static final String DELETE_EMPLOYE_BY_ID = "DELETE FROM EMPLOYES WHERE ID = ?";

    /**
     * The SQL query to insert an employee
     */
    public static final String INSERT_EMPLOYEE = "INSERT INTO EMPLOYES (NOM , PRENOM , TELDOM , TELPORT , TELPRO, ADRESSE, CODEPOSTAL , VILLE , EMAIL) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

    /**
     * The SQL query to update an employee
     */
    public static final String UPDATE_EMPLOYEE_BY_ID = "UPDATE EMPLOYES SET NOM = ?, PRENOM = ?, TELDOM = ?, TELPORT = ?, TELPRO = ?, ADRESSE = ?, CODEPOSTAL = ?, VILLE = ?, EMAIL = ? WHERE ID = ?";

    /**
     * Path to home (login) page
     */
    public static final String LOGINPAGE_JSP = "WEB-INF/LoginPage.jsp";

    /**
     * Path to welcome (list of all employess) page
     */
    public static final String WELCOMEPAGE_JSP = "WEB-INF/MainPage.jsp";

    /**
     * Path to details (add or update an employee) page 
     */
    public static final String ADDORUPDATEPAGE_JSP = "WEB-INF/AddOrUpdateInfo.jsp";

    /**
     * Path to exit (logout) page
     */
    public static final String LOGOUT_JSP = "WEB-INF/Logout.jsp";
    
    /**
     * Error message when login orpassword missing
     */
    public static final String ERROR_FIELD_MISSING = "<p class=\"text-danger\"> Vous devez renseigner les deux champs</p>";
    /**
     * Error message when login or password wrong
     */
    public static final String ERROR_LOGINS_WRONG = "<p class=\"text-danger\">Echec de la connexion! Verifiez votre login et/ou mot de passe et essayez à nouveau</p>";
}
