/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ElPackegito.Controller;

import static ElPackegito.Extra.Constantes.*;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ElPackegito.Model.DBActions;
import ElPackegito.Model.User;
import ElPackegito.Model.Employee;

/**
 *
 * @author Alexis
 */
public class Control extends HttpServlet{
    
    HttpSession session;
    DBActions dBAction;
    User selectedUser;
    String dBURL;
    String dBUser;
    String dBPassword;
    Employee selectedEmployee;

    /**
     * Execute diferent task depending from the request
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Retrieve session of the user
        session = request.getSession();
        
        dBURL = DATABASE_URL;
        dBUser = DATABASE_LOGIN;
        dBPassword = DATABASE_PASSWORD;
        // Retrieve user from the session
        selectedUser = (User) session.getAttribute("user");
        
        if (null == request.getParameter("action")) {
            //If user is connected
            if (selectedUser != null) {
                request.setAttribute("employeesList", dBAction.getEmployees());
                request.setAttribute("deleteSucceed", "-1");
                request.getRequestDispatcher(WELCOMEPAGE_JSP).forward(request, response);
            } else {
                request.getRequestDispatcher(LOGINPAGE_JSP).forward(request, response);
            }
        } 
        
        dBAction = new DBActions(dBURL, dBUser, dBPassword);
        switch (request.getParameter("action")) {
            case "login":
                if(request.getParameter("login").isEmpty() || request.getParameter("password").isEmpty()){
                    request.setAttribute("errKey", ERROR_FIELD_MISSING); 
                    request.getRequestDispatcher(LOGINPAGE_JSP).forward(request, response); 
                }
                else {
                    User userInput = dBAction.checkIfLoginAndPasswordAreRight(request.getParameter("login"), request.getParameter("password"));
                    if(userInput != null){
                        selectedUser = userInput;
                        session.setAttribute("user", selectedUser);
                        request.setAttribute("employeesList", dBAction.getEmployees());
                            request.setAttribute("deleteSucceed", "-1");
                        request.getRequestDispatcher(WELCOMEPAGE_JSP).forward(request, response);
                    }
                    else{
                        request.setAttribute("errKey", ERROR_LOGINS_WRONG);
                        request.getRequestDispatcher(LOGINPAGE_JSP).forward(request, response);
                    }
                }
                break;
            case "details":
                if (selectedUser != null) {
                    selectedEmployee = dBAction.getEmployeeByID(request.getParameter("id"));
                    if (selectedEmployee != null) {
                        request.setAttribute("add", false);
                        request.setAttribute("emp", selectedEmployee);
                        request.getRequestDispatcher(ADDORUPDATEPAGE_JSP).forward(request, response);
                    } else {
                        request.setAttribute("errKey", "Employee non trouvé");
                            request.setAttribute("deleteSucceed", "-1");
                        request.getRequestDispatcher(WELCOMEPAGE_JSP).forward(request, response);
                    }
                } else {
                    request.getRequestDispatcher(LOGINPAGE_JSP).forward(request, response);
                }   break;
            case "supprimer":
                if (selectedUser != null) {
                    // We check if the user is admin, else we redirect it to the welcome page
                    if (selectedUser.getIsAdmin()) {
                        if(dBAction.deleteEmployeeByID(request.getParameter("id"))){
                            request.setAttribute("deleteSucceed", "1");
                            request.setAttribute("employeesList", dBAction.getEmployees());
                            request.getRequestDispatcher(WELCOMEPAGE_JSP).forward(request, response);
                        }
                        else {
                            request.setAttribute("deleteSucceed", "0");
                            request.setAttribute("employeesList", dBAction.getEmployees());
                            request.getRequestDispatcher(WELCOMEPAGE_JSP).forward(request, response);
                        }
                    } else {
                        request.setAttribute("employeesList", dBAction.getEmployees());
                            request.setAttribute("deleteSucceed", "-1");
                        request.getRequestDispatcher(WELCOMEPAGE_JSP).forward(request, response);
                    }
                } else {
                    request.getRequestDispatcher(LOGINPAGE_JSP).forward(request, response);
                }   break;
            
            case "ajouter":
                if (selectedUser != null) {
                    if (selectedUser.getIsAdmin()) {
                        request.setAttribute("add", true);
                        request.getRequestDispatcher(ADDORUPDATEPAGE_JSP).forward(request, response);
                    } else {
                        request.setAttribute("employeesList", dBAction.getEmployees());
                            request.setAttribute("deleteSucceed", "-1");
                        request.getRequestDispatcher(WELCOMEPAGE_JSP).forward(request, response);
                    }
                } else {
                    request.getRequestDispatcher(LOGINPAGE_JSP).forward(request, response);
                }   break;
            case "reLogIn":
                request.getRequestDispatcher(LOGINPAGE_JSP).forward(request, response);
                break;
            case "modifier":
                if (selectedUser != null) {
                    if (selectedUser.getIsAdmin()) {
                        selectedEmployee = new Employee();
                        selectedEmployee.setId(Integer.parseInt(request.getParameter("id")));
                        selectedEmployee.setPrenom(request.getParameter("prenom"));
                        selectedEmployee.setNom(request.getParameter("nom"));
                        selectedEmployee.setTelDom(request.getParameter("telDom"));
                        selectedEmployee.setTelPort(request.getParameter("telPort"));
                        selectedEmployee.setTelPro(request.getParameter("telPro"));
                        selectedEmployee.setAdresse(request.getParameter("adresse"));
                        selectedEmployee.setCodePostal(request.getParameter("codePostal"));
                        selectedEmployee.setVille(request.getParameter("ville"));
                        selectedEmployee.setEmail(request.getParameter("email"));
                        dBAction.updateEmployee(selectedEmployee);
                        request.setAttribute("employeesList", dBAction.getEmployees());
                            request.setAttribute("deleteSucceed", "-1");
                        request.getRequestDispatcher(WELCOMEPAGE_JSP).forward(request, response);
                    }
                } else {
                    request.getRequestDispatcher(LOGINPAGE_JSP).forward(request, response);
                }   break;
            case "valAjout":
                if (selectedUser != null) {
                    if (selectedUser.getIsAdmin()) {
                        selectedEmployee = new Employee();
                        selectedEmployee.setPrenom(request.getParameter("prenom"));
                        selectedEmployee.setNom(request.getParameter("nom"));
                        selectedEmployee.setTelDom(request.getParameter("telDom"));
                        selectedEmployee.setTelPort(request.getParameter("telPort"));
                        selectedEmployee.setTelPro(request.getParameter("telPro"));
                        selectedEmployee.setAdresse(request.getParameter("adresse"));
                        selectedEmployee.setCodePostal(request.getParameter("codePostal"));
                        selectedEmployee.setVille(request.getParameter("ville"));
                        selectedEmployee.setEmail(request.getParameter("email"));
                        dBAction.addEmployee(selectedEmployee);
                        request.setAttribute("employeesList", dBAction.getEmployees());
                            request.setAttribute("deleteSucceed", "-1");
                        request.getRequestDispatcher(WELCOMEPAGE_JSP).forward(request, response);
                    }
                } else {
                    request.getRequestDispatcher(LOGINPAGE_JSP).forward(request, response);
                }   break;
            case "logOut":
                session.invalidate();
                request.getRequestDispatcher(LOGOUT_JSP).forward(request, response);
                break;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns description of the servlet.
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
