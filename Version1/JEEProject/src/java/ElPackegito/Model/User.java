/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ElPackegito.Model;

import java.util.ArrayList;

/**
 *
 * @author Alexis
 */
public class User {
    private String login;
    private String password;
    private Boolean isAdmin;
    
    public User(){
        
    }

    /**
     * Get of login
     * @return login
     */
    public String getLogin() {
        return login;
    }
    
    /**
     * Get of isAdmin 
     * @return isAdmin 
     */
    public Boolean getIsAdmin() {
        return isAdmin;
    }

    /**
     * Get password 
     * @return password 
     */
    public String getPassword() {
        return password;
    }
    
    /**
     * Set login 
     * @param login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Set password
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Set status
     * @param isAdmin
     */
    public void setIsAdmin(Boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

}
