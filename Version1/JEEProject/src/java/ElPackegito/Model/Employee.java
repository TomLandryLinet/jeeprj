/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ElPackegito.Model;

/**
 *
 * @author Alexis
 */
public class Employee {
    private int id;
    private String nom;
    private String prenom;
    private String telDom;
    private String telPort;
    private String telPro;
    private String adresse;
    private String codePostal;
    private String ville;
    private String email;

    /**
     * Get of id
     * @return id
     */
    public int getId() {
        return id;
    }
    
    /**
     * Get of nom
     * @return nom
     */
    public String getNom() {
        return nom;
    }
    
    /**
     * Get of telDom
     * @return telDom
     */
    public String getTelDom() {
        return telDom;
    }
    
    /**
     * Get of telPort
     * @return telPort
     */
    public String getTelPort() {
        return telPort;
    }
    
    /**
     * Get of telPro
     * @return telPro
     */
    public String getTelPro() {
        return telPro;
    }

    /**
     * Get of adresse
     * @return adresse
     */
    public String getAdresse() {
        return adresse;
    }
    
    /**
     * Get of email
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Get of codePostal
     * @return codePostal
     */
    public String getCodePostal() {
        return codePostal;
    }

    /**
     * Get of ville
     * @return ville
     */
    public String getVille() {
        return ville;
    }
    
    /**
     * Get of prenom
     * @return prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Set prenom
     * @param prenom
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Set id
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Set nom
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Set telDom
     * @param telDom
     */
    public void setTelDom(String telDom) {
        this.telDom = telDom;
    }

    /**
     * Set telPort
     * @param telPort
     */
    public void setTelPort(String telPort) {
        this.telPort = telPort;
    }

    /**
     * Set telPro
     * @param telPro
     */
    public void setTelPro(String telPro) {
        this.telPro = telPro;
    }

    /**
     * Set adresse
     * @param adresse
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
    
    /**
     * Set codePostal
     * @param codePostal
     */
    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    /**
     * Set ville
     * @param ville
     */
    public void setVille(String ville) {
        this.ville = ville;
    }

    /**
     * Set email
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }
}
