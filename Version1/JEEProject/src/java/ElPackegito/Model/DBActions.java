/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ElPackegito.Model;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import static ElPackegito.Extra.Constantes.*;

/**
 *
 * @author Alexis
 */
public class DBActions {
    Connection connection;
    ResultSet resultSet;
    Statement statement;
    PreparedStatement preparedStatement;

    /**
     * Constructor with 3 parameters
     * @param url
     * @param user
     * @param pwd 
     */
    public DBActions(String url, String user, String password) {
        try {
            connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }


    /**
     * Get statement from the connection
     * @param conn
     */
    public Statement getStatement(Connection connection) {
        try {
            statement = connection.createStatement();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return statement;
    }

    /**
     * Return ResultSet from a request
     * @param stmt
     * @param request
     */
    public ResultSet getResultSet(Statement statment, String request) {
        try {
            resultSet = statment.executeQuery(request);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return resultSet;
    }

    /**
     * Return the list of all user stored in the database
     */
    public ArrayList<User> getUser() {
        ArrayList<User> listOfUsers = new ArrayList<>();
        resultSet = getResultSet(getStatement(connection), SELECT_ALL_USERS);
        try {
            while (resultSet.next()) {
                User u = new User();
                u.setLogin(resultSet.getString("LOGIN"));
                u.setPassword((resultSet.getString("PASSWORD")));
                u.setIsAdmin(resultSet.getBoolean("ISADMIN"));
                listOfUsers.add(u);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return listOfUsers;
    }
    
    /**
     * Return a User if exist in the database search by login and password
     * @param login
     * @param pass
     */
    public User checkIfLoginAndPasswordAreRight(String login, String pass){
        ArrayList<User> allUsers = getUser();
        User returnedUser = null;
        for(int i = 0; i < allUsers.size(); i++){
            if(allUsers.get(i).getLogin().equals(login) && allUsers.get(i).getPassword().equals(pass)){
                returnedUser = allUsers.get(i);
            }
        }
        return returnedUser;
    }

    /**
     * Get all the employees in the databse
     */
    public ArrayList<Employee> getEmployees() {
        ArrayList<Employee> listOfEmployees = new ArrayList<>();
        resultSet = getResultSet(getStatement(connection), SELECT_ALL_EMPLOYES);
        try {
            while (resultSet.next()) {
                Employee e = new Employee();
                e.setId(resultSet.getInt("ID"));
                e.setNom(resultSet.getString("NOM"));
                e.setPrenom(resultSet.getString("PRENOM"));
                e.setTelDom(resultSet.getString("TELDOM"));
                e.setTelPort(resultSet.getString("TELPORT"));
                e.setTelPro(resultSet.getString("TELPRO"));
                e.setAdresse(resultSet.getString("ADRESSE"));
                e.setCodePostal(resultSet.getString("CODEPOSTAL"));
                e.setVille(resultSet.getString("VILLE"));
                e.setEmail(resultSet.getString("EMAIL"));
                listOfEmployees.add(e);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return listOfEmployees;
    }

    /**
     * Return a Employee if exist in the database search by id
     * @param id
     */
    public Employee getEmployeeByID(String id) {
        if(id != null){
            try {
                preparedStatement = connection.prepareStatement(SELECT_EMPLOYE_BY_ID);
                preparedStatement.setInt(1, Integer.parseInt(id));
                preparedStatement.executeQuery();
                resultSet = preparedStatement.getResultSet();
                while (resultSet.next()) {
                    Employee e = new Employee();
                    e.setId(resultSet.getInt("ID"));
                    e.setNom(resultSet.getString("NOM"));
                    e.setPrenom(resultSet.getString("PRENOM"));
                    e.setTelDom(resultSet.getString("TELDOM"));
                    e.setTelPort(resultSet.getString("TELPORT"));
                    e.setTelPro(resultSet.getString("TELPRO"));
                    e.setAdresse(resultSet.getString("ADRESSE"));
                    e.setCodePostal(resultSet.getString("CODEPOSTAL"));
                    e.setVille(resultSet.getString("VILLE"));
                    e.setEmail(resultSet.getString("EMAIL"));
                    return e;
                }
            } catch (SQLException ex) {
                Logger.getLogger(DBActions.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * Delete an employee search by id
     * @param id
     */
    public Boolean deleteEmployeeByID(String id) {
        if(id != null){
            try {
                preparedStatement = connection.prepareStatement(DELETE_EMPLOYE_BY_ID);
                preparedStatement.setInt(1, Integer.parseInt(id));
                preparedStatement.executeUpdate();
                return true;
            } catch (SQLException ex) {
                Logger.getLogger(DBActions.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        }
        return false;
    }

    /**
     * Prepare the Statement for an Insert or Update query
     * @param emp
     * @param prepStat
     */
    public boolean completeInsertOrUpdateQuery(Employee employee, PreparedStatement preparedStat) throws SQLException {
        if (employee.getNom().isEmpty() ||
                employee.getPrenom().isEmpty() ||
                employee.getTelDom().isEmpty() ||
                employee.getTelPort().isEmpty() ||
                employee.getTelPro().isEmpty() ||
                employee.getAdresse().isEmpty() ||
                employee.getCodePostal().isEmpty() ||
                employee.getVille().isEmpty() ||
                employee.getEmail().isEmpty()) 
        {
            return false;
        }
        preparedStat.setString(1, employee.getNom());
        preparedStat.setString(2, employee.getPrenom());
        preparedStat.setString(3, employee.getTelDom());
        preparedStat.setString(4, employee.getTelPort());
        preparedStat.setString(5, employee.getTelPro());
        preparedStat.setString(6, employee.getAdresse());
        preparedStat.setString(7, employee.getCodePostal());
        preparedStat.setString(8, employee.getVille());
        preparedStat.setString(9, employee.getEmail());
        return true;
    }

    /**
     * Add an employee to EMPLOYES
     * @param emp
     */
    public void addEmployee(Employee employee) {
        try {
            preparedStatement = (PreparedStatement) connection.prepareStatement(INSERT_EMPLOYEE);
            completeInsertOrUpdateQuery(employee, preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DBActions.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
 
    /**
     * Update an employee
     * @param emp
     */
    public void updateEmployee(Employee employee) {
        try {
            preparedStatement = connection.prepareStatement(UPDATE_EMPLOYEE_BY_ID);
            completeInsertOrUpdateQuery(employee, preparedStatement);
            preparedStatement.setInt(10, employee.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DBActions.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
