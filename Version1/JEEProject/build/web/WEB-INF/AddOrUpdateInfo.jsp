<%-- 
    Document   : AddOrUpdateInfo
    Created on : 17 nov. 2019, 23:52:06
    Author     : Alexis
--%>

<%@page import="ElPackegito.Model.User"%>
<%@page import="ElPackegito.Model.Employee"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>JSP Page</title>
    </head>
    <body>
        <nav class="navbar bg-light">
            <% User u = (User) request.getSession().getAttribute("user");
            %>
            <div class=" w-100 text-right">Bonjour <b><%=u.getLogin()%></b> ! Votre session est active 
                <form method="POST" action="Control">
                    <button class="btn btn-dark" type="submit" name="exit" value="deconnexion">X</button>
                </form>
            </div>
        </nav>
        <div class="container w-75">
            <%
                    Boolean isCreateUser = (Boolean) request.getAttribute("add");
                    String isDisabled = "disabled";
                    if(u.getIsAdmin()) isDisabled = "";
                    Employee em = (Employee) request.getAttribute("emp");
                    if (em!=null) {
                    request.setAttribute("employeeId", em.getId());
                %>
                <h3>Détails du membre <%=em.getPrenom()%> <%=em.getNom()%></h3>
                <%
                    } else {
                       em = new Employee();
                %>
                <h3>Ajouter employé</h3>
                <%
                    }
                %>
            <form class="text-right" method="POST" action="Control">
                
                <hr>
                <input type="hidden" name="id" value="<%=em.getId()%>">
                <div class="input-group pt-4">
                    <label class="col-2 col-form-label font-weight-bold">Nom</label>
                    <input class="form-control rounded" type="text" id="nom" value="<%if(!isCreateUser){%><%=em.getNom()%><%}%>" name="nom" <%=isDisabled %> required>
                </div>
                <div class="input-group pt-3">
                    <label class="col-2 col-form-label font-weight-bold">Prénom</label>
                    <input class="form-control rounded" type="text" id="prenom" value="<%if(!isCreateUser){%><%=em.getPrenom()%><%}%>" name="prenom" <%=isDisabled %> required>
                </div>
                <div class="input-group pt-3">
                    <label class="col-2 col-form-label font-weight-bold">Tel Dom</label>
                    <input class="form-control rounded" type="tel" id="telDom" value="<%if(!isCreateUser){%><%=em.getTelDom()%><%}%>" name="telDom" <%=isDisabled %> required>
                </div>
                <div class="input-group pt-3">
                    <label class="col-2 col-form-label font-weight-bold">Tel Pro</label>
                    <input class="form-control rounded" type="tel" id="telPro" value="<%if(!isCreateUser){%><%=em.getTelPro()%><%}%>" name="telPro" <%=isDisabled %> required>
                </div>
                <div class="input-group pt-3">
                    <label class="col-2 col-form-label font-weight-bold">Tel Mob</label>
                    <input class="form-control rounded" type="tel" id="telPort" value="<%if(!isCreateUser){%><%=em.getTelPort()%><%}%>" name="telPort" <%=isDisabled %> required>
                </div>
                <div class="input-group pt-3">
                    <label class="col-2 col-form-label font-weight-bold">Adresse</label>
                    <input class="form-control rounded" type="text" id="adresse" value="<%if(!isCreateUser){%><%=em.getAdresse()%><%}%>" name="adresse" <%=isDisabled %> required>
                
                    <label class="col-2 col-form-label font-weight-bold">Code Postal</label>
                    <input class="form-control rounded" type="text" id="codePostal" value="<%if(!isCreateUser){%><%=em.getCodePostal()%><%}%>" name="codePostal" <%=isDisabled %> required>
                </div>
                <div class="input-group pt-3">
                    <label class="col-2 col-form-label font-weight-bold">Ville</label>
                    <input class="form-control rounded" type="text" id="ville" value="<%if(!isCreateUser){%><%=em.getVille()%><%}%>" name="ville" <%=isDisabled %> required>
                
                    <label class="col-2 col-form-label font-weight-bold">Email</label>
                    <input class="form-control rounded" type="email" id="email" value="<%if(!isCreateUser){%><%=em.getEmail()%><%}%>" name="email" <%=isDisabled %> required>
                </div>
                <div class="pt-4">
                    <%
                        if (isCreateUser) {
                    %>
                    <button class="btn btn-primary rounded" type="submit" name="action" value="valAjout" <%=isDisabled%>>Valider</button>
                    <%
                        } else {
                    %>
                    <button class="btn btn-primary rounded" type="submit" name="action" value="modifier" <%=isDisabled%>>Modifier</button>
                    <%
                        }
                    %>
                    <a href="Control">
                        <button class="btn btn-light rounded" type="button" name="action" value="cancel">Voir liste</button>
                    </a>
                </div>
            </form>
        </div>   
    </body>
</html>
