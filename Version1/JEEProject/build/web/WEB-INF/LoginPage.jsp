<%-- 
    Document   : LoginPage
    Created on : 17 nov. 2019, 21:07:13
    Author     : Alexis
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>JSP Page</title>
    </head>
    <body>
        <h4 class="text-primary">Connexion</h4>
        <div class="w-50 mx-auto">
            <%
                String err = (String) request.getAttribute("errKey");
                if (err != null){
                    out.print(err);
                }
            %>
        </div>
        <form class="rounded border border-secondary w-50 mx-auto" action="Control" method="POST">
            <div class="bg-light py-1 pl-3">Login</div>
            <div class= "border border-secondary">
                <div class="input-group px-3 pt-3">
                  <input class="form-control" type="text" name="login" placeholder="Login">
                </div>
                <div class="input-group px-3 pt-3">
                  <input class="form-control" type="password" name="password" placeholder="Mot de passe">
                </div>
                <div class=" pl-3 py-3">
                    <button class="btn btn-primary rounded" type="submit" name="action" value="login">Login</button>
                </div>
            </div>
        </form>
    </body>
</html>
