<%-- 
    Document   : AddOrUpdateInfo
    Created on : 17 nov. 2019, 23:52:06
    Author     : Alexis
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>JSP Page</title>
    </head>
    <body>
        <nav class="navbar bg-light">
            <div class=" w-100 text-right">
            <c:if  test="${user ne null}">
                <p>Bonjour <b>${ user.login}</b> ! Votre session est active </p>
            </c:if>
                <form method="POST" action="Control">
                    <button class="btn btn-dark" type="submit" name="action" value="deconnexion">X</button>
                </form>
            </div>
        </nav>
        <div class="container w-75">
            <c:if  test="${selectedEmployee ne null}">
                    <h3>Détails du membre ${selectedEmployee.prenom} ${selectedEmployee.nom}</h3>
            </c:if>
            <c:if  test="${selectedEmployee eq null}">
                <h3>Ajouter employé</h3>
            </c:if>
            <form class="text-right" method="POST" action="Control">
                
                <hr>
                <input type="hidden" name="id" value="${selectedEmployee.id}">
                <div class="input-group pt-4">
                    <label class="col-2 col-form-label font-weight-bold">Nom</label>
                    <input class="form-control rounded" type="text" id="nom" value="${selectedEmployee.nom}" name="nom" <c:if  test="${!user.isAdmin}">Disabled </c:if> required>
                </div>
                <div class="input-group pt-3">
                    <label class="col-2 col-form-label font-weight-bold">Prénom</label>
                    <input class="form-control rounded" type="text" id="prenom" value="${selectedEmployee.prenom}" name="prenom" <c:if  test="${!user.isAdmin}">Disabled </c:if> required>
                </div>
                <div class="input-group pt-3">
                    <label class="col-2 col-form-label font-weight-bold">Tel Dom</label>
                    <input class="form-control rounded" type="tel" id="telDom" value="${selectedEmployee.teldom}" name="telDom" <c:if  test="${!user.isAdmin}">Disabled </c:if> required>
                </div>
                <div class="input-group pt-3">
                    <label class="col-2 col-form-label font-weight-bold">Tel Pro</label>
                    <input class="form-control rounded" type="tel" id="telPro" value="${selectedEmployee.telpro}" name="telPro" <c:if  test="${!user.isAdmin}">Disabled </c:if> required>
                </div>
                <div class="input-group pt-3">
                    <label class="col-2 col-form-label font-weight-bold">Tel Mob</label>
                    <input class="form-control rounded" type="tel" id="telPort" value="${selectedEmployee.telport}" name="telPort" <c:if  test="${!user.isAdmin}">Disabled </c:if> required>
                </div>
                <div class="input-group pt-3">
                    <label class="col-2 col-form-label font-weight-bold">Adresse</label>
                    <input class="form-control rounded" type="text" id="adresse" value="${selectedEmployee.adresse}" name="adresse" <c:if  test="${!user.isAdmin}">Disabled </c:if> required>
                
                    <label class="col-2 col-form-label font-weight-bold">Code Postal</label>
                    <input class="form-control rounded" type="text" id="codePostal" value="${selectedEmployee.codepostal}" name="codePostal" <c:if  test="${!user.isAdmin}">Disabled </c:if> required>
                </div>
                <div class="input-group pt-3">
                    <label class="col-2 col-form-label font-weight-bold">Ville</label>
                    <input class="form-control rounded" type="text" id="ville" value="${selectedEmployee.ville}" name="ville" <c:if  test="${!user.isAdmin}">Disabled </c:if> required>
                
                    <label class="col-2 col-form-label font-weight-bold">Email</label>
                    <input class="form-control rounded" type="email" id="email" value="${selectedEmployee.email}" name="email" <c:if  test="${!user.isAdmin}">Disabled </c:if> required>
                </div>
                <div class="pt-4">
                    <c:if  test="${!empty selectedEmployee}">
                        <button class="btn btn-primary rounded" type="submit" name="action" value="modifier" <c:if  test="${!user.isAdmin}">Disabled </c:if>>Modifier</button>
                    </c:if>
                    <c:if  test="${empty selectedEmployee}">
                        <button class="btn btn-primary rounded" type="submit" name="action" value="valAjout" <c:if  test="${!user.isAdmin}">Disabled </c:if>>Valider</button>
                    </c:if>
                    
                    <a href="Control">
                        <button class="btn btn-light rounded" type="button" name="action" value="cancel">Voir liste</button>
                    </a>
                </div>
            </form>
        </div>   
    </body>
</html>
